const examData = [
    { id: 1, name: "Tony", score: 87 },
    { id: 2, name: "Steve", score: 91 },
    { id: 3, name: "Scott", score: 72 },
    { id: 4, name: "Natasha", score: 66 },
    { id: 5, name: "Bruce", score: 77 },
    { id: 6, name: "Denvers", score: 82 },
    { id: 7, name: "Pepper", score: 91 },
    { id: 8, name: "Clint", score: 84 },
    { id: 9, name: "Barton", score: 90 },
    { id: 10, name: "Stacey", score: 88 },
    { id: 11, name: "Wanda", score: 50 },
    { id: 12, name: "Peter", score: 79 },
    { id: 13, name: "James", score: 84 },
    { id: 14, name: "Shang", score: 85 },
];
  
// You can modify the variable name
let averageScore = (data) => {
    let jumlah = data.reduce((total, value) => total + value.score, 0);
    let hasil = jumlah / examData.length;
    return hasil.toFixed(0);
}

let highestStudents = (data) => {
    let result = "";
    let rataAtas = data.filter(tertinggi => tertinggi.score > averageScore(examData));
    let high = rataAtas.sort((a, b) => b.score - a.score);
    for(let i = 0; i < high.length; i++) {
        if(i > 0 && high[i].score == high[0].score && high[i+1].score != high[0].score) {
            result += high[i].name + ".";
        } else if(high[i].score == high[0].score) {
            result += high[i].name + ", ";
        }  
    }
    return result;
}

let lowestStudents = (data) => {
    let rataBawah = data.filter(terendah => terendah.score < averageScore(examData));
    let high = rataBawah.sort((a, b) => a.score - b.score);
    return high[0].name;
}


let failStudents = (data) => {
    let result = "";
    let i;
    let fail = data.filter(failed => failed.score < 75);
    for(i = 0; i < fail.length; i++) {
        if(i == fail.length-1) {
            result += fail[i].name + ".";
        } else {
            result += fail[i].name + ", ";
        }  
    }
    return result;
}

let passPercentage = (data) => {
    let success = data.filter(graduate => graduate.score > 75);
    let percent = success.length / data.length * 100;
    return `${percent.toFixed(2)}%`;
}
  
console.log(`Average score: ${averageScore(examData)}`);
console.log(`Student who get highest score: ${highestStudents(examData)}`);
console.log(`Student who get highest score: ${lowestStudents(examData)}`);
console.log(`Student who fail: ${failStudents(examData)}`);
console.log(`Pass percentage: ${passPercentage(examData)}`);
  